package view.bikeRentalView;

/**
 * BikeRentalMain contains  public static void main(String[] args) method.
 * BikeRentalView object is created to call setUpBikeRental() method,
 * which is used to start the program.
 */

public class BikeRentalMain {
    public static void main(String[] args) {
        BikeRentalView bikeRentalView = new BikeRentalView();
        bikeRentalView.setUpBikeRental();
    }


}
