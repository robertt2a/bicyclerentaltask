package view.bikeRentalView;

import controller.bike.BikeController;
import controller.bike.BikeControllerImpl;
import controller.bikeBase.BikeBaseController;
import controller.bikeBase.BikeBaseControllerImpl;
import controller.customer.CustomerController;
import controller.customer.CustomerControllerImpl;
import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import model.bike.Bike;
import model.customer.Customer;

import java.util.InputMismatchException;
import java.util.Map;

/**
 * This is bike rental service program.
 * It's main function is to handle the rental and returning of bikes.
 * In the begining customer is made with given name and amount of money in wallet.
 * Each has seven parameters, which define:
 *        -price per hour of bike rental
 *        -bike colour,
 *        -bike brand,
 *        -bike type,
 *        -time when the rental started,
 *        -customer which rented bike,
 *        -status of bike (is available for rent or not).
 * Bike brand and bike type can be "premium type" and they are increasing price.
 * Base of bikes is randomly generated with random size and bikes which parameters
 * are drawn from dedicated enums.
 * Basic price per hour of rental was set as 5$.
 * Price can be increased according to other bike parameters.
 * The program displays menu with 11 options:
 *        A: Show all bikes - displays base of bikes
 *        B: Book a bike - allows customer to choose and book bike, if it is available
 *        C: Filter bikes by colour - displays base, with bikes of given colour on the top
 *        D: Filter bikes by brand - displays base, with bikes of given brand on the top
 *        E: Filter bikes by availability - allow customer to choose if he want to display
 *          available or unavailable bikes
 *        F: Sort bikes by price - displays base sorted by price in ascending or descending
 *              order according to customers choice
 *        G: Show your rented bikes - displays all bikes rented by customer
 *        H: Return your rented bike - displays all bikes rented by customer and
 *           allows him to choose which one he want to return,
 *          also it returns bike to rental and charges a rental fee
 *        I: Check your amount of money - allows customer to check his amount of money
 *        J: Transfer money to your wallet - allows customer to transfer money into wallet
 *        K: Close the program - checks if there is debit in customers wallet, and if
 *          customer returned all bikes to rental, if both requirements
 *          are met, the program is closed.
 * Detailed description of particular methods can be found in the corresponding classes.
 */

public class BikeRentalView {
    public void setUpBikeRental() {
        boolean runTheProgram = true;
        CustomerController customerController = new CustomerControllerImpl();
        BikeController bikeController = new BikeControllerImpl();
        BikeBaseController bikeBaseController = new BikeBaseControllerImpl();
        ScannerController scannerController = new ScannerControllerImpl();
        Customer customer;
        while (true) {
            customer = customerController.createNewCustomer();
            if (customer == null) {
                System.out.println("Invalid input, try to create user again");
            } else {
                break;
            }
        }
        Map<Integer, Bike> bikeBaseMap = bikeBaseController.createBikeBase();
        while (runTheProgram) {
            System.out.println("*********************\r\n"
                    + "Welcome in our Bike Rental!\r\n"
                    + "Choose option: (enter letter related to option)\r\n"
                    + "A: Show all bikes\r\n"
                    + "B: Book a bike\r\n"
                    + "C: Filter bikes by colour\r\n"
                    + "D: Filter bikes by brand\r\n"
                    + "E: Filter bikes by availability\r\n"
                    + "F: Sort bikes by price\r\n"
                    + "G: Show your rented bikes\r\n"
                    + "H: Return your rented bike\r\n"
                    + "I: Check your amount of money\r\n"
                    + "J: Transfer money to your wallet\r\n"
                    + "K: Close the program\r\n"
                    + "*********************\r\n");
            try {
                String chooseOption = scannerController.nextLine().toUpperCase();
                if (chooseOption.equals("")) {
                    throw new InputMismatchException("Invalid input, choose one of given options");
                }
                switch (chooseOption) {
                    case "A":
                        bikeBaseController.showBikeBase(bikeBaseMap);
                        break;
                    case "B":
                        bikeController.bookBike(bikeBaseMap, customer);
                        break;
                    case "C":
                        bikeBaseController.filterBikesByColour(bikeBaseMap);
                        break;
                    case "D":
                        bikeBaseController.filterBikesByBrand(bikeBaseMap);
                        break;
                    case "E":
                        bikeBaseController.filterBikesByStatus(bikeBaseMap);
                        break;
                    case "F":
                        bikeBaseController.sortBikesByPrice(bikeBaseMap);
                        break;
                    case "G":
                        Map<Integer, Bike> customersBikesBase = bikeController.createCustomersBookedBikesMap(bikeBaseMap);
                        bikeController.showCustomersBookedBikes(customersBikesBase);
                        break;
                    case "H":
                        bikeController.returnBikeToRental(bikeBaseMap, customer);
                        break;
                    case "I":
                        customerController.showWalletContent(customer);
                        break;
                    case "J":
                        customerController.transferMoneyIntoWallet(customer);
                        break;
                    case "K":
                        Map<Integer, Bike> actualCustomersBikesBase = bikeController.createCustomersBookedBikesMap(bikeBaseMap);
                        boolean isDebitInWallet = customerController.isDebitInWallet(customer);
                        if (!actualCustomersBikesBase.isEmpty()) {
                            System.out.println("You have to return all rented bikes before closing the program!");
                        }
                        if (actualCustomersBikesBase.isEmpty() && !isDebitInWallet) {
                            runTheProgram = false;
                            System.out.println("Thank you for your visit! Have a nice day!");
                        }
                        break;
                    default:
                        System.out.println("Invalid option choice, try again.");
                }
            } catch (InputMismatchException exception) {
                System.out.println(exception.getMessage() + " Closing the program");
            }
        }
    }
}
