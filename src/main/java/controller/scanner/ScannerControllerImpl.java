package controller.scanner;

import java.util.Scanner;

public class ScannerControllerImpl implements ScannerController {
    @Override
    public String nextLine() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
