package controller.scanner;

/**
 * Simple interface with implementation used for testing purposes.
 * ScannerController allows to create unit tests for methods which uses Scanner to read user input
 * It allows to use Mockito methods on Scanner
 */

public interface ScannerController {
    String nextLine();
}
