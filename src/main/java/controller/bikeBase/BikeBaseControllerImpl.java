package controller.bikeBase;

import controller.bike.BikeController;
import controller.bike.BikeControllerImpl;
import controller.randomWrapper.RandomWrapper;
import controller.randomWrapper.RandomWrapperImpl;
import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import lombok.NoArgsConstructor;
import model.bike.Bike;
import model.bike.BikeBrand;
import model.bike.BikeColour;

import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor
public class BikeBaseControllerImpl implements BikeBaseController {
    private RandomWrapper randomWrapper = new RandomWrapperImpl();
    private ScannerController scannerController = new ScannerControllerImpl();

    public BikeBaseControllerImpl(RandomWrapper random) {
        this.randomWrapper = random;
    }

    public BikeBaseControllerImpl(ScannerController scannerController) {
        this.scannerController = scannerController;
    }

    @Override
    public Map<Integer, Bike> createBikeBase() {
        BikeController bikeController = new BikeControllerImpl();
        Integer key = 1;
        Map<Integer, Bike> bikeMap = new HashMap<>();
        for (int i = 0; i < randomWrapper.nextInt(10, 10); i++) {
            bikeMap.put(key, bikeController.createNewBike());
            key++;
        }
        return bikeMap;
    }

    @Override
    public void showBikeBase(Map<Integer, Bike> bikeMap) {
        bikeMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(System.out::println);
    }

    @Override
    public void filterBikesByColour(Map<Integer, Bike> bikeMap) {
        List<String> bikeColoursNamesList = new ArrayList<>();
        for (BikeColour colour : BikeColour.values()) {
            bikeColoursNamesList.add(colour.getColourName().toUpperCase());
        }
        try {
            System.out.println("Choose one of available colours: red, green, blue, black, white, yellow, orange.");
            String givenColor = scannerController.nextLine().toUpperCase();
            if (givenColor.equals("")) {
                throw new IllegalArgumentException("Invalid colour choice - you have to type colour name");
            }
            if (bikeColoursNamesList.contains(givenColor)) {
                Map<Integer, Bike> temporaryMap = bikeMap.entrySet()
                        .stream()
                        .filter(bikeEntry -> bikeEntry.getValue().getBikeColour().getColourName().toUpperCase().equals(givenColor))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                if (temporaryMap.isEmpty()){
                    System.out.println("We don't have bikes in this colour at this moment, please check our other bikes:");
                } else {
                    temporaryMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByKey())
                            .forEach(System.out::println);
                }
                bikeMap.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.comparing(Bike::getBikeColour,Comparator.naturalOrder())))
                        .filter(bikeEntry -> !bikeEntry.getValue().getBikeColour().getColourName().toUpperCase().equals(givenColor))
                        .forEach(System.out::println);
            } else {
                throw new IllegalArgumentException("Invalid colour choice - choose one of available colours");
            }
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public void filterBikesByBrand(Map<Integer, Bike> bikeMap) {
        List<String> bikeBrandsNamesList = new ArrayList<>();
        for (BikeBrand brand : BikeBrand.values()) {
            bikeBrandsNamesList.add(brand.getBrandName().toUpperCase());
        }
        try {
            System.out.println("Choose one of available brands: Romet, Kross, Trek, Scott.");
            String givenBrand = scannerController.nextLine().toUpperCase();
            if (givenBrand.equals("")) {
                throw new IllegalArgumentException("Invalid brand choice - you have to type brand name");
            }
            if (bikeBrandsNamesList.contains(givenBrand)) {
                Map<Integer, Bike> temporaryMap =bikeMap.entrySet()
                        .stream()
                        .filter(bikeEntry -> bikeEntry.getValue().getBikeBrand().getBrandName().toUpperCase().equals(givenBrand))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                if (temporaryMap.isEmpty()){
                    System.out.println("We don't have bikes of this brand at this moment, please check our other bikes:");
                } else {
                    temporaryMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByKey())
                            .forEach(System.out::println);
                }
                bikeMap.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.comparing(Bike::getBikeBrand, Comparator.naturalOrder())))
                        .filter(bikeEntry -> !bikeEntry.getValue().getBikeBrand().getBrandName().toUpperCase().equals(givenBrand))
                        .forEach(System.out::println);

            } else {
                throw new IllegalArgumentException("Invalid brand choice - choose one of available brands");
            }
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public void filterBikesByStatus(Map<Integer, Bike> bikeMap) {
        System.out.println("Choose bike status: \r\nA: available\r\nB: unavailable");
        try {
            String chosenStatus = scannerController.nextLine().toUpperCase();
            if (!chosenStatus.equals("A") && !chosenStatus.equals("B")) {
                throw new IllegalArgumentException("Invalid choice");
            }
            switch (chosenStatus) {
                case "A":
                    System.out.println("Currently available bikes: ");
                    bikeMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByKey())
                            .filter(bikeEntry -> bikeEntry.getValue().getBikeStatus().isAvailable())
                            .forEach(System.out::println);
                    break;
                case "B":
                    System.out.println("Currently unavailable bikes: ");
                    bikeMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByKey())
                            .filter(bikeEntry -> !bikeEntry.getValue().getBikeStatus().isAvailable())
                            .forEach(System.out::println);
                    break;
            }
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public void sortBikesByPrice(Map<Integer, Bike> bikeMap) {
        System.out.println("Choose sorting order: \r\nA: ascending\r\nB: descending");
        try {
            String sortingOrder = scannerController.nextLine().toUpperCase();
            if (!sortingOrder.equals("A") && !sortingOrder.equals("B")) {
                throw new IllegalArgumentException("Invalid choice");
            }
            switch (sortingOrder) {
                case "A":
                    bikeMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByValue(Comparator.comparing(Bike::getPrice)))
                            .forEach(System.out::println);
                    break;
                case "B":
                    bikeMap.entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByValue(Comparator.comparing(Bike::getPrice).reversed()))
                            .forEach(System.out::println);
                    break;
            }
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
