package controller.bikeBase;

import model.bike.Bike;

import java.util.Map;

/**
 * BikeBaseController is responsible for creating map of bikes, which will be basis of program's operation. It also
 * contains method to print and filter given map:
 * - createBikeBase() - returns new map of bikes, uses RandomWrapper class nextInt() method to generate size of map
 *                      (value between 10(inclusive) and 20(exclusive)). Then it iterates over created map, adding
 *                      pairs key-value where key is Integer (key starts from 1 and is incremented inside for loop)
 *                      and value is new Bike object which is created by calling createNewBike method from BikeController
 * - showBikeBase(Map<Integer,Bike> bikeMap) - this method displays given map in the console, sorted ascending by keys
 * - filterBikesByColour(Map<Integer,Bike> bikeMap) - this method displays given map in the console, filtered by given
 *                      colour, entered by user. Bikes with colour parameter equal to given colour are displayed on the
 *                      top. Below rest of the bikes are displayed below (as in the task description), sorted by colour name
 *                      value (in order defined in enum). If there is no bike with given color, all bikes are displayed
 *                      sorted by colour name value (in order defined in enum).
 * - filterBikesByBrand(Map<Integer,Bike> bikeMapt) - this method displays given map in the console, filtered by given
 *                      brand, entered by user. Bikes with brand parameter equal to given brand are displayed on the
 *                      top. Rest of the bikes are displayed below (as in the task description), sorted by brand name
 *                      value (in order defined in enum). If there is no bike with given brand, all bikes are displayed
 *                      sorted by brand name value (in order defined in enum).
 * - filterBikesByStatus(Map<Integer,Bike> bikeMap - this method displays given map in the console, filtered by bike
 *                      avialability, user have to choose if he wants to display available or unavailable bikes
 * - sortBikesByPrice(Map<Integer,Bike> bikeMap) - this method displays given map in the console, sorted by bike
 *                      price. User have to choose if he want to sort it with ascending or descending order
 */

public interface BikeBaseController {
    Map<Integer, Bike> createBikeBase();
    void showBikeBase(Map<Integer,Bike> bikeMap);
    void filterBikesByColour(Map<Integer,Bike> bikeMap);
    void filterBikesByBrand(Map<Integer,Bike> bikeMapt);
    void filterBikesByStatus(Map<Integer,Bike> bikeMap);
    void sortBikesByPrice(Map<Integer,Bike> bikeMap);
}
