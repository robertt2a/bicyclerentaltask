package controller.bike;

import model.bike.Bike;
import model.customer.Customer;
import java.time.LocalTime;
import java.util.Map;

/**
 * BikeController is responsible for creating new bikes, booking and then returning them to base.
 * It has 8 methods:
 * - createNewBike() - uses BikeBuilder to create new Bike object and calculatePrice(Bike bike) method to update
 *                    it's price according to other parameters, then it returns created bike
 *                    It also uses chooseRandomElementFromEnum(Class<T> enumType) as a BikeBuilder parameter,
 *                    to set random values for Bike parameters
 * - calculatePrice(Bike bike) - sets new Bike price increased by price ratio of premium Brand or/and Type
 * - chooseRandomElementFromEnum(Class<T> enumType) - uses Random object to return random value from given Enum
 * - bookBike(Map<Integer, Bike> bikeMap, Customer customer) - takes map of bikes and Customer object as parameters
 *                    uses BikeBaseController object and showBikeBase method to display given map, it allows user
 *                    to choose which Bike he want to book, and if it's available, uses Bike.setCustomer() to set
 *                    given customer as a Bike tenant. In the end it sets actual time as a Bike rent time
 * - returnBikeToRental(Map<Integer,Bike> bikeMap, Customer customer) - takes map of bikes and Customer object as
 *                    parameters and creates a new Map using createCustomersBookedBikesMap method to get map of
 *                    bikes rented by given customer, then uses showCustomersBookedBikes method to print it.
 *                    It allows user to choose which Bike he want to return, then (if input is valid) returns
 *                    chosen Bike to rental by setting its customer parameter as null
 *                    In the end it uses chargeCustomer method to charge rental fee and sets Bike parameter
 *                    rentTime as null
 * - chargeCustomer(Customer customer, LocalTime rentStartTime, double price) - takes Customer object, LocalTime object
 *                    and primitive double as parameters. Methods reads actual time and calculates difference between actual
 *                    and given time, and saves it as a rent duration value. Then it reads actual wallet content from given
 *                    Customer object and sets new money value by subtraction: actual money value minus given price multiplied
 *                    by rent duration. We assume that one hour of rent is equal to 10 seconds (for testing purposes)
 *                    LocalTime is a final class and cannot be mocked, so instead using LocalTime.now() there is need to use
 *                    Clock.systemUTC() as a parameter for testing purposes. Clock.systemUTC() returns actual time and it can be
 *                    mocked so it is possible to create unit tests.
 * - createCustomersBookedBikesMap(Map<Integer,Bike> bikeMap) - filters given map and returns new map
 *                    with Bikes which Customer parameter is not null. It uses stream to filter and collect values.
 *                    Method only checks if Bike parameter customer parameter is not null because of program construction -
 *                    there is no possibility that there is more than one Customer object, and during creation of Bike object
 *                    this parameter is set as null. Only way to set customer value as not null is calling bookBike method.
 *                    During the program only one Customer object is created, so there is no risk that this method will return
 *                    Bikes assigned to other customer.
 * - showCustomersBookedBikes(Map<Integer,Bike> bikeMap) - displays given map in the console
 **/

public interface BikeController {
    Bike createNewBike();
    void calculatePrice(Bike bike);
    <T extends Enum<T>> T chooseRandomElementFromEnum(Class<T> enumType);
    void bookBike(Map<Integer,Bike> bikeMap, Customer customer);
    void returnBikeToRental(Map<Integer,Bike> bikeMap, Customer customer);
    void chargeCustomer(Customer customer, LocalTime rentStartTime, double price);
    Map<Integer,Bike> createCustomersBookedBikesMap(Map<Integer,Bike> bikeMap);
    void showCustomersBookedBikes(Map<Integer,Bike> bikeMap);
}
