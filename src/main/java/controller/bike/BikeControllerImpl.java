package controller.bike;

import controller.bikeBase.BikeBaseController;
import controller.bikeBase.BikeBaseControllerImpl;
import controller.randomWrapper.RandomWrapper;
import controller.randomWrapper.RandomWrapperImpl;
import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import lombok.NoArgsConstructor;
import model.bike.*;
import model.bike.bikeBuilder.BikeBuilder;
import model.bike.bikeBuilder.BikeBuilderImpl;
import model.customer.Customer;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Project lombok annotation @NoArgsConstructor is used to override default constructor without unnecessary "boilerplate code".
 * BikeControllerImpl constructor with ScannerController parameter was created only for testing purposes.
 * Method descriptions can be found in BikeController interface.
 */

@NoArgsConstructor
public class BikeControllerImpl implements BikeController {
    private RandomWrapper randomWrapper = new RandomWrapperImpl();
    private ScannerController scannerController = new ScannerControllerImpl();

    public BikeControllerImpl(RandomWrapper random) {
        this.randomWrapper = random;
    }

    BikeControllerImpl(ScannerController scannerController) {
        this.scannerController = scannerController;
    }

    @Override
    public Bike createNewBike() {
        BikeBuilder bikeBuilder = new BikeBuilderImpl();
        bikeBuilder.setPrice(5);
        bikeBuilder.setBikeBrand(chooseRandomElementFromEnum(BikeBrand.class));
        bikeBuilder.setBikeColour(chooseRandomElementFromEnum(BikeColour.class));
        bikeBuilder.setBikeType(chooseRandomElementFromEnum(BikeType.class));
        bikeBuilder.setBikeStatus(chooseRandomElementFromEnum(BikeStatus.class));
        bikeBuilder.setCustomer(null);
        bikeBuilder.setRentTime(null);
        Bike newBike = bikeBuilder.build();
        calculatePrice(newBike);
        return newBike;
    }

    @Override
    public void calculatePrice(Bike bike) {
        double newPrice = bike.getPrice() * bike.getBikeBrand().getPriceRatio() * bike.getBikeType().getPriceRatio();
        bike.setPrice(newPrice);
    }


    @Override
    public <T extends Enum<T>> T chooseRandomElementFromEnum(Class<T> enumType) {
        List<T> enumValues = Arrays.asList(enumType.getEnumConstants());
        return enumValues.get(randomWrapper.nextInt(enumValues.size(),0));
    }

    @Override
    public void bookBike(Map<Integer, Bike> bikeMap, Customer customer) {
        BikeBaseController bikeBaseController = new BikeBaseControllerImpl();
        System.out.println("********************************");
        System.out.println("Bikes in our base:");
        bikeBaseController.showBikeBase(bikeMap);
        System.out.println("********************************");
        System.out.println("Which bike you want to rent? Enter bike id:");
        try {
            String chosenNumber = scannerController.nextLine();
            if (chosenNumber.equals("")) {
                throw new IllegalArgumentException("Bike number cannot be empty");
            }
            int bikeNumber = Integer.parseInt(chosenNumber);
            if (bikeMap.keySet().contains(bikeNumber)) {
                Bike chosenBike = bikeMap.get(bikeNumber);
                if (!chosenBike.getBikeStatus().isAvailable()) {
                    throw new IllegalArgumentException("This bike is unavailable now");
                }
                chosenBike.setBikeStatus(BikeStatus.UNAVAILABLE);
                chosenBike.setCustomer(customer);
                chosenBike.setRentTime(LocalTime.now(Clock.systemUTC()));
                System.out.println("Good choice! You rented " + chosenBike.toString() + ". Have a nice ride!");
            } else {
                throw new InputMismatchException("You have to choose number from base");
            }
        } catch (IllegalArgumentException | InputMismatchException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public void returnBikeToRental(Map<Integer, Bike> bikeMap, Customer customer) {
        Map<Integer, Bike> customersBikesMap = createCustomersBookedBikesMap(bikeMap);
        if (customersBikesMap.isEmpty()) {
            System.out.println("You have not rented any bike yet.");
        } else {
            showCustomersBookedBikes(customersBikesMap);
            System.out.println("Which bike you want to return? Enter bike id:");
            try {
                String chosenNumber = scannerController.nextLine();
                if (chosenNumber.equals("")) {
                    throw new IllegalArgumentException("Bike number cannot be empty");
                }
                int bikeNumber = Integer.parseInt(chosenNumber);
                if (customersBikesMap.keySet().contains(bikeNumber)) {
                    Bike chosenBike = bikeMap.get(bikeNumber);
                    double bikeRentPrice = chosenBike.getPrice();
                    chosenBike.setCustomer(null);
                    LocalTime rentStartTime = chosenBike.getRentTime();
                    chosenBike.setRentTime(null);
                    chargeCustomer(customer, rentStartTime, bikeRentPrice);
                    chosenBike.setBikeStatus(BikeStatus.AVAILABLE);
                    System.out.println("Bike " + chosenBike.toString() + " successfully returned.");
                }
            } catch (InputMismatchException | IllegalArgumentException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

    @Override
    public void chargeCustomer(Customer customer, LocalTime rentStartTime, double price) {
        LocalTime rentEndTime = LocalTime.now(Clock.systemUTC());
        double rentDuration = Duration.between(rentStartTime, rentEndTime).getSeconds() / 10;
        double actualMoney = customer.getWallet().getMoney();
        customer.getWallet().setMoney(actualMoney - (price * rentDuration));
        System.out.println("You used bike for " + rentDuration +" hours. Price per hour is " + price);
        System.out.println("A fee of $ " + price*rentDuration + " has been charged");
    }

    @Override
    public void showCustomersBookedBikes(Map<Integer, Bike> bikeMap) {
        if (bikeMap.isEmpty()) {
            System.out.println("You have not rented any bike yet.");
        } else {
            System.out.println("Your rented bikes:");
            bikeMap.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(System.out::println);
        }
    }

    @Override
    public Map<Integer, Bike> createCustomersBookedBikesMap(Map<Integer, Bike> bikeMap) {
        return bikeMap.entrySet()
                .stream()
                .filter(bikeEntry -> bikeEntry.getValue().getCustomer() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
