package controller.randomWrapper;

import java.util.Random;

/**
 * nextInt method has two int parameters and uses Random().nextInt method to generate random number between
 * specified bound (given as first method parameter) and 0. The range can be "moved" by adding value to both bounds - this value is represented
 * by "extra" parameter
 */

public class RandomWrapperImpl implements RandomWrapper {
    @Override
    public int nextInt(int bound, int extra) {
        return new Random().nextInt(bound) + extra;
    }
}
