package controller.randomWrapper;

/**
 * Simple interface with implementation used for testing purposes.
 * It allows to create unit tests for methods which uses Random class objects to generate random numbers
 */

public interface RandomWrapper {
  int nextInt(int bound, int extra);
}
