package controller.customer;

import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import model.customer.Customer;
import model.customer.Wallet;

import java.util.InputMismatchException;


@AllArgsConstructor
@NoArgsConstructor
public class CustomerControllerImpl implements CustomerController {
    ScannerController scannerController = new ScannerControllerImpl();

    @Override
    public Customer createNewCustomer() throws NumberFormatException {
        try {
            System.out.println("Hello, enter your user name: ");
            String name = scannerController.nextLine();
            if (name.equals("")) {
                throw new IllegalArgumentException("Name cannot be empty");
            }
            System.out.println("How much money you want to spend on renting a bike?");
            String enteredMoneyValue = scannerController.nextLine();
            if (enteredMoneyValue.equals("")) {
                throw new IllegalArgumentException("Money value cannot be empty");
            }
            double money = Double.parseDouble(enteredMoneyValue);
            return new Customer(new Wallet(money), name);
        } catch (IllegalArgumentException | InputMismatchException exception) {
            System.out.println(exception.getMessage());
            return null;
        }
    }

    @Override
    public void showWalletContent(Customer customer) {
        System.out.println("Your actual wallet content: " + customer.getWallet().getMoney() + " $");

    }

    @Override
    public void transferMoneyIntoWallet(Customer customer) {
        try {
            showWalletContent(customer);
            System.out.println("How much money you want to transfer?");
            String enteredMoneyValue = scannerController.nextLine();
            if (enteredMoneyValue.equals("")) {
                throw new IllegalArgumentException();
            }
            double transferedMoney = Double.parseDouble(enteredMoneyValue);
            if (transferedMoney>0) {
                customer.getWallet().setMoney(customer.getWallet().getMoney() + transferedMoney);
                showWalletContent(customer);
            } else {
                System.out.println("You can't withdraw money from your wallet");
            }
        } catch (IllegalArgumentException | InputMismatchException exception) {
            System.out.println("Invalid input, try again");
        }
    }

    @Override
    public boolean isDebitInWallet(Customer customer) {
        if (customer.getWallet().getMoney() < 0) {
            System.out.println("There is debit on your wallet: " + customer.getWallet().getMoney() + "$. \r\n"
                    + "You have to transfer some money.");
        }

        return customer.getWallet().getMoney() < 0;
    }
}
