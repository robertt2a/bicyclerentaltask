package controller.customer;

import model.customer.Customer;

/**
 * CustomerController is responsible for creating customer and all actions regarding wallet
 * It contains four methods:
 * - createNewCustomer() - creates Customer object using ScannerController object for input. Creates new Wallet with
 *                      entered money value. Method use try-catch block to check if input is correct and returns null
 *                      if isn't.
 * - showWalletContent(Customer customer) - simple method which displays amount of money in Wallet for given Customer
 * - transferMoneyIntoWallet(Customer customer)- uses showWalletContent to display amount of money in Wallet and
 *                      allows user to add money to Wallet
 * - isDebitInWallet(Customer customer) - checks if amount of money in Wallet is lesser than 0. If it is, program
 *                      cannot be closed.
 */

public interface CustomerController {
    Customer createNewCustomer();
    void showWalletContent(Customer customer);
    void transferMoneyIntoWallet(Customer customer);
    boolean isDebitInWallet(Customer customer);
}
