package model.bike.bikeBuilder;

import model.bike.*;
import model.customer.Customer;
import java.time.LocalTime;

public class BikeBuilderImpl implements BikeBuilder {
    private Bike bike;

    public BikeBuilderImpl(){
        bike = new Bike();
    }

    @Override
    public Bike build() {
        return bike;
    }

    @Override
    public BikeBuilder setPrice(double price) {
        bike.setPrice(price);
        return this;
    }

    @Override
    public BikeBuilder setBikeStatus(BikeStatus bikeStatus) {
        bike.setBikeStatus(bikeStatus);
        return this;
    }

    @Override
    public BikeBuilder setCustomer(Customer customer) {
        bike.setCustomer(customer);
        return this;
    }

    @Override
    public BikeBuilder setBikeType(BikeType bikeType) {
        bike.setBikeType(bikeType);
        return this;
    }

    @Override
    public BikeBuilder setBikeColour(BikeColour bikeColour) {
        bike.setBikeColour(bikeColour);
        return this;
    }

    @Override
    public BikeBuilder setBikeBrand(BikeBrand bikeBrand) {
        bike.setBikeBrand(bikeBrand);
        return this;
    }

    @Override
    public BikeBuilder setRentTime(LocalTime rentTime) {
        bike.setRentTime(rentTime);
        return this;
    }
}
