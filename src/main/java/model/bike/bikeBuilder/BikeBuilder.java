package model.bike.bikeBuilder;

import model.bike.*;
import model.customer.Customer;
import java.time.LocalTime;

/**
 * BikeBuilder is an interface for BikeBuilderImpl, which contains implementation of Builder design pattern for Bike class.
 */

public interface BikeBuilder {
    Bike build();
    BikeBuilder setPrice(double price);
    BikeBuilder setBikeStatus(BikeStatus bikeStatus);
    BikeBuilder setCustomer(Customer customer);
    BikeBuilder setBikeType(BikeType bikeType);
    BikeBuilder setBikeColour(BikeColour bikeColour);
    BikeBuilder setBikeBrand(BikeBrand bikeBrand);
    BikeBuilder setRentTime(LocalTime rentTime);
}
