package model.bike;

/**
 * BikeType enum stores information about bike type
 * It contains two parameters:
 * priceRatio - determines how much the price of renting a bicycle will be increased
 *              there are three "premium" types of bikes, which rent price is bigger,
 *              and one "normal" type with priceRatio=1 (price won't be increased)
 * typeName - stores name of bike type
 *
 * This Enum uses lombok in order to avoid unnecessary "boilerplate code"
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BikeType {

    MOUNTAIN_BIKE(1.5,"Mountain bike"),
    ROAD_BIKE(1.5, "Road bike"),
    CITY_BIKE(1, "City bike"),
    ELECTRIC_BIKE(2, "Electric bike");

    private double priceRatio;
    private String typeName;

}
