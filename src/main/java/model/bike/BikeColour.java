package model.bike;

/**
 * BikeColour enum stores information about bike colour
 * It will be used to create various Bike objects
 * It stores only one parameter - colourName to describe each BikeColour value
 *
 * This Enum uses lombok in order to avoid unnecessary "boilerplate code"
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BikeColour {
    RED("Red"),
    GREEN("Green"),
    BLUE("Blue"),
    BLACK("Black"),
    WHITE("White"),
    YELLOW("Yellow"),
    ORANGE("Orange");

    private String colourName;

}
