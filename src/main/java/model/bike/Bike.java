package model.bike;

/**
 * Bike class describes parameters of Bike object which are essential for the entire project
 * It also contains overrided toString() method
 * Project lombok is used in order to avoid unnecessary "boilerplate code".
 */

import lombok.Getter;
import lombok.Setter;
import model.customer.Customer;

import java.time.LocalTime;


@Setter
@Getter
public class Bike {
    private double price;
    private BikeStatus bikeStatus;
    private Customer customer;
    private BikeType bikeType;
    private BikeColour bikeColour;
    private BikeBrand bikeBrand;
    private LocalTime rentTime;

    @Override
    public String toString() {
        return "Bike(" +
                "price=" + price +
                ", bikeStatus=" + bikeStatus +
                ", bikeType=" + bikeType +
                ", bikeColour=" + bikeColour +
                ", bikeBrand=" + bikeBrand +
                ')';
    }
}
