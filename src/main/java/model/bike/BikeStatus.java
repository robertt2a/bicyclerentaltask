package model.bike;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * BikeStatus enum stores information about Bike availability for renting
 *
 * This Enum uses lombok in order to avoid unnecessary "boilerplate code"
 */

@Getter
@AllArgsConstructor
public enum BikeStatus {
    AVAILABLE(true),
    UNAVAILABLE(false);

    private boolean available;
}
