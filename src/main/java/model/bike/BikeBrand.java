package model.bike;
/**
 * BikeBrand enum stores information about bike brandIt contains two parameters:
 * priceRatio - determines how much the price of renting a bicycle will be increased - more recognizable brand
 *              means better quality and higher price
 * brandName
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BikeBrand {

    ROMET("Romet",1),
    KROSS("Kross",1.2),
    TREK("Trek",1.5),
    SCOTT("Scott",1.7);

    private String brandName;
    private double priceRatio;
}
