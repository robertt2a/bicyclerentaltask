package model.customer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Customer class stores information about customers name and reference to customers Wallet
 *
 * This class uses project lombok annotations in order to avoid unnecessary "boilerplate code"
 */

@Getter
@Setter
@AllArgsConstructor
public class Customer {
    private Wallet wallet;
    private String name;
}
