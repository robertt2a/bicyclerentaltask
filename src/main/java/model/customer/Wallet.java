package model.customer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Wallet - simple class with one parameter money which stores information about customers amount od money
 *
 * This class uses project lombok annotations in order to avoid unnecessary "boilerplate code"
 */

@Getter
@Setter
@AllArgsConstructor
public class Wallet {
    private double money;
}
