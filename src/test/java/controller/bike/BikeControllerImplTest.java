package controller.bike;

import controller.randomWrapper.RandomWrapper;
import controller.randomWrapper.RandomWrapperImpl;
import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import model.bike.*;
import model.customer.Customer;
import model.customer.Wallet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Clock.class, BikeControllerImpl.class})
public class BikeControllerImplTest {
    private ScannerController mockedScannerController = mock(ScannerControllerImpl.class);
    private RandomWrapper mockedRandomWrapper = mock(RandomWrapperImpl.class);
    private BikeController underTestWithScanner = new BikeControllerImpl(mockedScannerController);
    private BikeController underTestWithRandom = new BikeControllerImpl(mockedRandomWrapper);


    @Test
    public void shouldReturnNotNullObject() {
        underTestWithScanner.chooseRandomElementFromEnum(BikeBrand.class);
        assertNotNull(underTestWithScanner.chooseRandomElementFromEnum(BikeBrand.class));
    }

    @Test
    public void shouldReturnBikeColourObject() {
        assertTrue(Arrays.asList(BikeColour.values()).contains(underTestWithScanner.chooseRandomElementFromEnum(BikeColour.class)));
    }

    @Test
    public void shouldReturnExpectedBikeColourValue() {
        when(mockedRandomWrapper.nextInt(7,0)).thenReturn(3);
        BikeColour testColour = underTestWithRandom.chooseRandomElementFromEnum(BikeColour.class);
        assertEquals(BikeColour.BLACK,testColour);

    }
    @Test
    public void shouldReturnExpectedBikeTypeValue() {
        when(mockedRandomWrapper.nextInt(4,0)).thenReturn(2);
        BikeType testType = underTestWithRandom.chooseRandomElementFromEnum(BikeType.class);
        assertEquals(BikeType.CITY_BIKE,testType);
    }

    @Test
    public void shouldReturnExpectedBikeStatusValue() {

        when(mockedRandomWrapper.nextInt(2,0)).thenReturn(1);
        BikeStatus testStatus = underTestWithRandom.chooseRandomElementFromEnum(BikeStatus.class);
        verify(mockedRandomWrapper, times(1)).nextInt(2,0);
        assertEquals(BikeStatus.UNAVAILABLE,testStatus);
    }
    @Test
    public void shouldReturnExpectedBikeBrandValue() {
        when(mockedRandomWrapper.nextInt(4,0)).thenReturn(3);
        BikeBrand testBrand = underTestWithRandom.chooseRandomElementFromEnum(BikeBrand.class);
        assertEquals(BikeBrand.SCOTT,testBrand);
    }

    @Test
    public void shouldCalculateBikePriceDueToGivenParameters() {
        Bike testBike = new Bike();
        testBike.setPrice(10);
        testBike.setBikeBrand(BikeBrand.TREK);
        testBike.setBikeType(BikeType.ELECTRIC_BIKE);
        underTestWithScanner.calculatePrice(testBike);
        assertEquals(30, testBike.getPrice(), 0.01);
    }

    @Test
    public void shouldCreateNewBikeObjectWithRandomParameters() {
        Bike testBike = underTestWithScanner.createNewBike();

        assertNotNull(testBike.getBikeType());
        assertTrue(Arrays.asList(BikeType.values()).contains(testBike.getBikeType()));

        assertNotNull(testBike.getBikeBrand());
        assertTrue(Arrays.asList(BikeBrand.values()).contains(testBike.getBikeBrand()));

        assertNotNull(testBike.getBikeColour());
        assertTrue(Arrays.asList(BikeColour.values()).contains(testBike.getBikeColour()));

        assertNull(testBike.getCustomer());
        assertNull(testBike.getRentTime());

        double expectedPrice = 5 * testBike.getBikeType().getPriceRatio() * testBike.getBikeBrand().getPriceRatio();
        assertEquals(expectedPrice, testBike.getPrice(), 0.01);

        assertTrue(Arrays.asList(BikeStatus.values()).contains(testBike.getBikeStatus()));
    }

    @Test
    public void shouldPrintGivenMap() {
        Map<Integer, Bike> testMap = new HashMap<>();
        Bike bike = new Bike();
        testMap.put(1, bike);
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.showCustomersBookedBikes(testMap);
        String expectedOutput = "Your rented bikes:\r\n" + "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldReturnMapWithBikesCustomersNotNullValues() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setCustomer(testCustomer);
        bike2.setCustomer(testCustomer);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        Map<Integer, Bike> mapReturnedFromMethod = underTestWithScanner.createCustomersBookedBikesMap(testMap);
        assertEquals(2, mapReturnedFromMethod.size());
        assertEquals(mapReturnedFromMethod.get(1), bike1);
        assertEquals(mapReturnedFromMethod.get(2), bike2);
        for (Bike bike : mapReturnedFromMethod.values()) {
            assertNotNull(bike.getCustomer());
        }
    }

    @Test
    public void shouldCalculateBikeRentPriceAndChargeCusomer() {
        Clock clock = Clock.fixed(Instant.parse("2019-08-02T12:00:00.00Z"), ZoneId.of("UTC"));
        Clock clock1 = Clock.fixed(Instant.parse("2019-08-02T12:00:50.00Z"), ZoneId.of("UTC"));
        mockStatic(Clock.class);
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        double testPrice = 5;
        when(Clock.systemUTC()).thenReturn(clock1);
        underTestWithScanner.chargeCustomer(testCustomer, LocalTime.now(clock), testPrice);
        assertEquals(75, testCustomer.getWallet().getMoney(), 0.01);
    }


    @Test
    public void shouldCreateDebitOnWallet() {
        Clock clock = Clock.fixed(Instant.parse("2019-08-02T12:00:00.00Z"), ZoneId.of("UTC"));
        Clock clock1 = Clock.fixed(Instant.parse("2019-08-02T12:00:50.00Z"), ZoneId.of("UTC"));
        mockStatic(Clock.class);
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        double testPrice = 25;
        when(Clock.systemUTC()).thenReturn(clock1);
        underTestWithScanner.chargeCustomer(testCustomer, LocalTime.now(clock), testPrice);
        assertEquals(-25, testCustomer.getWallet().getMoney(), 0.01);
    }

    @Test
    public void shouldReturnExceptionMessageFromReturnBikeMethod() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Bike bike1 = new Bike();

        bike1.setCustomer(testCustomer);

        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));

        underTestWithScanner.returnBikeToRental(testMap, testCustomer);

        String expectedOutput = "Your rented bikes:\r\n" + "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "Which bike you want to return? Enter bike id:\r\n" +
                "Bike number cannot be empty\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldReturnBikeToRental() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Clock clock = Clock.fixed(Instant.parse("2019-08-02T12:00:00.00Z"), ZoneId.of("UTC"));
        Clock clock1 = Clock.fixed(Instant.parse("2019-08-02T12:00:50.00Z"), ZoneId.of("UTC"));

        mockStatic(Clock.class);

        Bike bike1 = new Bike();

        bike1.setCustomer(testCustomer);
        bike1.setPrice(5);
        bike1.setRentTime(LocalTime.now(clock));

        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("1");
        when(Clock.systemUTC()).thenReturn(clock1);
        underTestWithScanner.returnBikeToRental(testMap, testCustomer);

        assertEquals(75, testCustomer.getWallet().getMoney(), 0.01);
        assertNull(bike1.getCustomer());
        assertNull(bike1.getRentTime());
        assertEquals(BikeStatus.AVAILABLE, bike1.getBikeStatus());
    }

    @Test
    public void shouldReturnExceptionMessageFromBookBikeMethodDueToEmptyInput() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Bike bike1 = new Bike();
        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));

        underTestWithScanner.bookBike(testMap, testCustomer);
        String expectedOutput = "********************************\r\n" +
                "Bikes in our base:\r\n" +
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "********************************\r\n" +
                "Which bike you want to rent? Enter bike id:\r\n" +
                "Bike number cannot be empty\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldReturnExceptionMessageFromBookBikeMethodDueToInvalidInput() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Bike bike1 = new Bike();
        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("2");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));

        underTestWithScanner.bookBike(testMap, testCustomer);
        String expectedOutput = "********************************\r\n" +
                "Bikes in our base:\r\n" +
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "********************************\r\n" +
                "Which bike you want to rent? Enter bike id:\r\n" +
                "You have to choose number from base\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldReturnExceptionMessageFromBookBikeMethodDueToUnavailableBike() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Bike bike1 = new Bike();
        bike1.setBikeStatus(BikeStatus.UNAVAILABLE);
        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("1");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));

        underTestWithScanner.bookBike(testMap, testCustomer);
        String expectedOutput = "********************************\r\n" +
                "Bikes in our base:\r\n" +
                "1=Bike(price=0.0, bikeStatus=UNAVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "********************************\r\n" +
                "Which bike you want to rent? Enter bike id:\r\n" +
                "This bike is unavailable now\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldBookChosenBike() {
        Customer testCustomer = new Customer(new Wallet(100), "testCustomer");
        Map<Integer, Bike> testMap = new HashMap<>();
        Clock clock = Clock.fixed(Instant.parse("2019-08-02T12:00:00.00Z"), ZoneId.of("UTC"));
        mockStatic(Clock.class);
        Bike bike1 = new Bike();
        bike1.setBikeStatus(BikeStatus.AVAILABLE);
        testMap.put(1, bike1);
        when(mockedScannerController.nextLine()).thenReturn("1");
        when(Clock.systemUTC()).thenReturn(clock);

        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.bookBike(testMap, testCustomer);

        String expectedOutput = "********************************\r\n" +
                "Bikes in our base:\r\n" +
                "1=Bike(price=0.0, bikeStatus=AVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "********************************\r\n" +
                "Which bike you want to rent? Enter bike id:\r\n" +
                "Good choice! You rented " + "Bike(price=0.0, bikeStatus=UNAVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)" + ". Have a nice ride!\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
        assertNotNull(bike1.getRentTime());
        assertNotNull(bike1.getCustomer());
        assertEquals(LocalTime.now(clock), bike1.getRentTime());
        assertEquals(testCustomer, bike1.getCustomer());
    }
}