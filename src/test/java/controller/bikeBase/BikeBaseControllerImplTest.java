package controller.bikeBase;


import controller.randomWrapper.RandomWrapper;
import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import model.bike.Bike;
import model.bike.BikeBrand;
import model.bike.BikeColour;
import model.bike.BikeStatus;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BikeBaseControllerImplTest {
    private RandomWrapper mockedRandomWrapper = mock(RandomWrapper.class);
    private ScannerController mockedScannerController = mock(ScannerControllerImpl.class);
    private BikeBaseController underTestWithScanner = new BikeBaseControllerImpl(mockedScannerController);
    private BikeBaseController underTestWithRandom = new BikeBaseControllerImpl(mockedRandomWrapper);

    @Test
    public void shouldReturnMapOfBikes() {
        Map<Integer, Bike> testBikeMap = underTestWithScanner.createBikeBase();
        assertTrue(testBikeMap.size() >= 10);
        for (Bike bike : testBikeMap.values()) {
            assertNotNull(bike);
        }
        Set<Integer> testKeySet = new HashSet<>();
        for (int i = 0; i < testBikeMap.size(); i++) {
            testKeySet.add(i + 1);
        }
        assertEquals(testKeySet, testBikeMap.keySet());
    }
    @Test
    public void shouldReturnMapOfGivenSize(){
        when(mockedRandomWrapper.nextInt(10,10)).thenReturn(15);
        Map<Integer, Bike> testBikeMap = underTestWithRandom.createBikeBase();
        assertEquals(15,testBikeMap.size());
    }

    @Test
    public void shouldPrintMapSortedByKeysProperly() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);


        ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.showBikeBase(testMap);

        String expectedOutput = "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "2=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "3=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "4=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldPrintMapFilteredByGivenColorName() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeColour(BikeColour.BLACK);
        bike2.setBikeColour(BikeColour.RED);
        bike3.setBikeColour(BikeColour.BLUE);
        bike4.setBikeColour(BikeColour.BLACK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);


        when(mockedScannerController.nextLine()).thenReturn("black");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByColour(testMap);

        String expectedOutput = "Choose one of available colours: red, green, blue, black, white, yellow, orange.\r\n" +
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLACK, bikeBrand=null)\r\n" +
                "4=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLACK, bikeBrand=null)\r\n" +
                "2=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=RED, bikeBrand=null)\r\n" +
                "3=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLUE, bikeBrand=null)\r\n";

        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldPrintMapSortedLexicographicallyByColourValue() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeColour(BikeColour.BLACK);
        bike2.setBikeColour(BikeColour.RED);
        bike3.setBikeColour(BikeColour.BLUE);
        bike4.setBikeColour(BikeColour.BLACK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);


        when(mockedScannerController.nextLine()).thenReturn("yellow");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByColour(testMap);

        String expectedOutput = "Choose one of available colours: red, green, blue, black, white, yellow, orange.\r\n" +
                "We don't have bikes in this colour at this moment, please check our other bikes:\r\n" +
                "2=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=RED, bikeBrand=null)\r\n" +
                "3=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLUE, bikeBrand=null)\r\n" +
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLACK, bikeBrand=null)\r\n" +
                "4=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=BLACK, bikeBrand=null)\r\n";

        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldThrowAndCatchExceptionDueToEmptyColourInput() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeColour(BikeColour.BLACK);
        bike2.setBikeColour(BikeColour.RED);
        bike3.setBikeColour(BikeColour.BLUE);
        bike4.setBikeColour(BikeColour.BLACK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByColour(testMap);

        String expectedOutput = "Choose one of available colours: red, green, blue, black, white, yellow, orange.\r\n" +
                "Invalid colour choice - you have to type colour name\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldThrowAndCatchExceptionDueToInvalidColourInput() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeColour(BikeColour.BLACK);
        bike2.setBikeColour(BikeColour.RED);
        bike3.setBikeColour(BikeColour.BLUE);
        bike4.setBikeColour(BikeColour.BLACK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("other colour");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByColour(testMap);

        String expectedOutput = "Choose one of available colours: red, green, blue, black, white, yellow, orange.\r\n" +
                "Invalid colour choice - choose one of available colours\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());

    }

    @Test
    public void shouldPrintMapFilteredByGivenBrandName() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeBrand(BikeBrand.KROSS);
        bike2.setBikeBrand(BikeBrand.TREK);
        bike3.setBikeBrand(BikeBrand.ROMET);
        bike4.setBikeBrand(BikeBrand.TREK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);


        when(mockedScannerController.nextLine()).thenReturn("trek");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByBrand(testMap);

        String expectedOutput = "Choose one of available brands: Romet, Kross, Trek, Scott.\r\n" +
                "2=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=TREK)\r\n" +
                "4=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=TREK)\r\n" +
                "3=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=ROMET)\r\n"+
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=KROSS)\r\n";

        assertEquals(expectedOutput, testOutputStream.toString());
    }
    @Test
    public void shouldPrintMapSortedLexicographicallyByBrandValue() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeBrand(BikeBrand.KROSS);
        bike2.setBikeBrand(BikeBrand.SCOTT);
        bike3.setBikeBrand(BikeBrand.ROMET);
        bike4.setBikeBrand(BikeBrand.SCOTT);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);


        when(mockedScannerController.nextLine()).thenReturn("trek");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByBrand(testMap);

        String expectedOutput = "Choose one of available brands: Romet, Kross, Trek, Scott.\r\n" +
                "We don't have bikes of this brand at this moment, please check our other bikes:\r\n" +
                "3=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=ROMET)\r\n"+
                "1=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=KROSS)\r\n" +
                "2=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=SCOTT)\r\n" +
                "4=Bike(price=0.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=SCOTT)\r\n";


        assertEquals(expectedOutput, testOutputStream.toString());
    }


    @Test
    public void shouldThrowAndCatchExceptionDueToEmptyBrandInput() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeBrand(BikeBrand.KROSS);
        bike2.setBikeBrand(BikeBrand.TREK);
        bike3.setBikeBrand(BikeBrand.ROMET);
        bike4.setBikeBrand(BikeBrand.TREK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByBrand(testMap);

        String expectedOutput = "Choose one of available brands: Romet, Kross, Trek, Scott.\r\n" +
                "Invalid brand choice - you have to type brand name\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldThrowAndCatchExceptionDueToInvalidBrandInput() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeBrand(BikeBrand.KROSS);
        bike2.setBikeBrand(BikeBrand.TREK);
        bike3.setBikeBrand(BikeBrand.ROMET);
        bike4.setBikeBrand(BikeBrand.TREK);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("other brand");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByBrand(testMap);

        String expectedOutput = "Choose one of available brands: Romet, Kross, Trek, Scott.\r\n" +
                "Invalid brand choice - choose one of available brands\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());

    }

    @Test
    public void shouldFilterAndPrintAvailableBikes() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeStatus(BikeStatus.AVAILABLE);
        bike2.setBikeStatus(BikeStatus.UNAVAILABLE);
        bike3.setBikeStatus(BikeStatus.AVAILABLE);
        bike4.setBikeStatus(BikeStatus.AVAILABLE);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("A");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByStatus(testMap);

        String expectedOutput = "Choose bike status: \r\nA: available\r\nB: unavailable\r\n" +
                "Currently available bikes: \r\n" +
                "1=Bike(price=0.0, bikeStatus=AVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "3=Bike(price=0.0, bikeStatus=AVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "4=Bike(price=0.0, bikeStatus=AVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldFilterAndPrintUnavailableBikes() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setBikeStatus(BikeStatus.AVAILABLE);
        bike2.setBikeStatus(BikeStatus.UNAVAILABLE);
        bike3.setBikeStatus(BikeStatus.AVAILABLE);
        bike4.setBikeStatus(BikeStatus.AVAILABLE);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("B");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByStatus(testMap);

        String expectedOutput = "Choose bike status: \r\nA: available\r\nB: unavailable\r\n" +
                "Currently unavailable bikes: \r\n" +
                "2=Bike(price=0.0, bikeStatus=UNAVAILABLE, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldThrowExceptionDueToInvalidStatusChoice() {
        Map<Integer, Bike> testMap = new HashMap<>();
        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.filterBikesByStatus(testMap);
        String expectedOutput = "Choose bike status: \r\nA: available\r\nB: unavailable\r\n" + "Invalid choice\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldThrowExceptionDueToInvalidPriceChoice() {
        Map<Integer, Bike> testMap = new HashMap<>();
        when(mockedScannerController.nextLine()).thenReturn("");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.sortBikesByPrice(testMap);
        String expectedOutput = "Choose sorting order: \r\n" +
                "A: ascending\r\n" +
                "B: descending\r\n" + "Invalid choice\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldPrintMapSortedByBikePriceAscending() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setPrice(2);
        bike2.setPrice(10);
        bike3.setPrice(7);
        bike4.setPrice(3);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        when(mockedScannerController.nextLine()).thenReturn("A");
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTestWithScanner.sortBikesByPrice(testMap);

        String expectedOutput = "Choose sorting order: \r\n" +
                "A: ascending\r\n" +
                "B: descending\r\n" +
                "1=Bike(price=2.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "4=Bike(price=3.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "3=Bike(price=7.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "2=Bike(price=10.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";

        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldPrintMapSortedByBikePriceDescending() {
        Map<Integer, Bike> testMap = new HashMap<>();

        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        Bike bike3 = new Bike();
        Bike bike4 = new Bike();

        bike1.setPrice(2);
        bike2.setPrice(10);
        bike3.setPrice(7);
        bike4.setPrice(3);

        testMap.put(1, bike1);
        testMap.put(3, bike3);
        testMap.put(2, bike2);
        testMap.put(4, bike4);

        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        when(mockedScannerController.nextLine()).thenReturn("B");
        underTestWithScanner.sortBikesByPrice(testMap);

        String expectedOutput = "Choose sorting order: \r\n" +
                "A: ascending\r\n" +
                "B: descending\r\n" +
                "2=Bike(price=10.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "3=Bike(price=7.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "4=Bike(price=3.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n" +
                "1=Bike(price=2.0, bikeStatus=null, bikeType=null, bikeColour=null, bikeBrand=null)\r\n";

        assertEquals(expectedOutput, testOutputStream.toString());
    }
}