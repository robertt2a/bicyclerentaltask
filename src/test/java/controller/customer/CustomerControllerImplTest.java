package controller.customer;

import controller.scanner.ScannerController;
import controller.scanner.ScannerControllerImpl;
import model.customer.Customer;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CustomerControllerImplTest {
    private ScannerController scannerControllerMock = mock(ScannerControllerImpl.class);
    private CustomerController underTest = new CustomerControllerImpl(scannerControllerMock);

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerException(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","");
        Customer testCustomer = underTest.createNewCustomer();
        underTest.transferMoneyIntoWallet(testCustomer);
    }

    @Test
    public void shouldReturnNullDueToNameValueInvalidInput(){
        when(scannerControllerMock.nextLine()).thenReturn("");
        Customer testCustomer = underTest.createNewCustomer();
        assertNull(testCustomer);
    }
    @Test
    public void shouldReturnNullDueToMoneyValueInvalidInput(){
        when(scannerControllerMock.nextLine()).thenReturn("name","");
        Customer testCustomer = underTest.createNewCustomer();
        assertNull(testCustomer);
    }

    @Test
    public void shouldCreateNewCustomer(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","100");
        Customer testCustomer = underTest.createNewCustomer();
        assertEquals(100,testCustomer.getWallet().getMoney(),0.01);
        assertEquals("testCustomer",testCustomer.getName());
    }

    @Test
    public void shouldReturnWalletContent(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","100");
        Customer testCustomer = underTest.createNewCustomer();
        OutputStream testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
        underTest.showWalletContent(testCustomer);
        String expectedOutput = "Your actual wallet content: 100.0 $\r\n";
        assertEquals(expectedOutput, testOutputStream.toString());
    }

    @Test
    public void shouldTransferMoneyIntoWallet(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","100","100");
        Customer testCustomer = underTest.createNewCustomer();
        underTest.transferMoneyIntoWallet(testCustomer);
        assertEquals(200,testCustomer.getWallet().getMoney(),0.01);
    }
    @Test
    public void shouldNotTransferMoneyIntoWalletDueToInvalidInput(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","100","");
        Customer testCustomer = underTest.createNewCustomer();
        underTest.transferMoneyIntoWallet(testCustomer);
        assertEquals(100,testCustomer.getWallet().getMoney(),0.01);
    }
    @Test
    public void shouldReturnTrueFromIsDebitInWallet(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","-100");
        Customer testCustomer = underTest.createNewCustomer();
        assertTrue(underTest.isDebitInWallet(testCustomer));
    }

    @Test
    public void shouldReturnFalseFromIsDebitInWallet(){
        when(scannerControllerMock.nextLine()).thenReturn("testCustomer","100");
        Customer testCustomer = underTest.createNewCustomer();
        assertFalse(underTest.isDebitInWallet(testCustomer));
    }
}