package model.bike.bikeBuilder;

import model.bike.*;
import model.customer.Customer;
import model.customer.Wallet;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class BikeBuilderImplTest {
    private BikeBuilder builderUnderTest = new BikeBuilderImpl();
    private Customer testCustomer = new Customer(new Wallet(0),"testCustomer");
    private LocalTime testTime = LocalTime.of(9,15,30);

    @Test
    public void shouldCreateBikeUsingBuilder(){
        builderUnderTest.setPrice(10);
        builderUnderTest.setBikeBrand(BikeBrand.ROMET);
        builderUnderTest.setBikeColour(BikeColour.BLACK);
        builderUnderTest.setBikeStatus(BikeStatus.AVAILABLE);
        builderUnderTest.setCustomer(testCustomer);
        builderUnderTest.setBikeType(BikeType.ELECTRIC_BIKE);
        builderUnderTest.setRentTime(testTime);

        Bike testBike = builderUnderTest.build();
        assertEquals(10,testBike.getPrice(),0.01);
        assertEquals(BikeBrand.ROMET,testBike.getBikeBrand());
        assertEquals(BikeColour.BLACK,testBike.getBikeColour());
        assertEquals(testCustomer,testBike.getCustomer());
        assertEquals(BikeType.ELECTRIC_BIKE,testBike.getBikeType());
        assertEquals(testTime,testBike.getRentTime());
        assertTrue(testBike.getBikeStatus().isAvailable());

    }
}