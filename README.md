# BicycleRentalTask
<br> This project was created in order to complete task for recruitment interview. 
<br> Project was created with with MVC pattern.
<br> Unit tests were created using JUnit4, Mockito and PowerMock 
<br> Program uses Project Lombok in order to avoid unnecessary "boilerplate code".

<br> This is bike rental service program.
<br> It's main function is to handle the rental and returning of bikes. 
<br> In the begining customer is made with given name and amount of money in wallet.
<br> Each has seven parameters, which define:
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -price per hour of bike rental
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -bike colour,
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -bike brand,
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -bike type,
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -time when the rental started,
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -customer which rented bike,
<br> &nbsp;&nbsp;&nbsp;&nbsp;   -status of bike (is available for rent or not).
<br> Bike brand and bike type can be "premium type" and they are increasing price.
<br> Base of bikes is randomly generated with random size and bikes which parameters
<br> are drawn from dedicated enums. 
<br> Basic price per hour of rental was set as 5$.
<br> Price can be increased according to other bike parameters.
<br> The program displays menu with 11 options:
<br> &nbsp;&nbsp;&nbsp;&nbsp;   A: Show all bikes - displays base of bikes
<br> &nbsp;&nbsp;&nbsp;&nbsp;   B: Book a bike - allows customer to choose and book bike, if it is available 
<br> &nbsp;&nbsp;&nbsp;&nbsp;   C: Filter bikes by colour - displays base, with bikes of given colour on the top
<br> &nbsp;&nbsp;&nbsp;&nbsp;   D: Filter bikes by brand - displays base, with bikes of given brand on the top
<br> &nbsp;&nbsp;&nbsp;&nbsp;   E: Filter bikes by availability - allow customer to choose if he want to display
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; available or unavailable bikes
<br> &nbsp;&nbsp;&nbsp;&nbsp;   F: Sort bikes by price - displays base sorted by price in ascending or descending 
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     order according to customers choice
<br> &nbsp;&nbsp;&nbsp;&nbsp;   G: Show your rented bikes - displays all bikes rented by customer
<br> &nbsp;&nbsp;&nbsp;&nbsp;   H: Return your rented bike - displays all bikes rented by customer and
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  allows him to choose which one he want to return,
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; also it returns bike to rental and charges a rental fee
<br> &nbsp;&nbsp;&nbsp;&nbsp;   I: Check your amount of money - allows customer to check his amount of money
<br> &nbsp;&nbsp;&nbsp;&nbsp;   J: Transfer money to your wallet - allows customer to transfer money into wallet
<br> &nbsp;&nbsp;&nbsp;&nbsp;   K: Close the program - checks if there is debit in customers wallet, and if 
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; customer returned all bikes to rental, if both requirements 
<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; are met, the program is closed.
<br> Detailed description of particular methods can be found in the corresponding classes.

                           